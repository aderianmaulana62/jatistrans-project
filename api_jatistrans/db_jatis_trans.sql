-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 27, 2019 at 09:30 AM
-- Server version: 10.3.15-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_jatis_trans`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_m_bus`
--

CREATE TABLE `tb_m_bus` (
  `bus_id` int(10) NOT NULL,
  `class_id` int(12) DEFAULT NULL,
  `workday_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_m_bus`
--

INSERT INTO `tb_m_bus` (`bus_id`, `class_id`, `workday_id`) VALUES
(1, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_m_class`
--

CREATE TABLE `tb_m_class` (
  `class_id` int(10) NOT NULL,
  `class_name` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_m_class`
--

INSERT INTO `tb_m_class` (`class_id`, `class_name`) VALUES
(1, 'VIP'),
(2, 'REGULER');

-- --------------------------------------------------------

--
-- Table structure for table `tb_m_location`
--

CREATE TABLE `tb_m_location` (
  `location_id` varchar(10) NOT NULL,
  `pool_id` varchar(20) DEFAULT NULL,
  `city_name` varchar(20) DEFAULT NULL,
  `pool_name_id` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_m_location`
--

INSERT INTO `tb_m_location` (`location_id`, `pool_id`, `city_name`, `pool_name_id`) VALUES
('01-JKT-LOC', NULL, 'Jakarta', '01-JKT');

-- --------------------------------------------------------

--
-- Table structure for table `tb_m_pool`
--

CREATE TABLE `tb_m_pool` (
  `pool_id` varchar(20) NOT NULL,
  `pool_name_id` varchar(20) DEFAULT NULL,
  `pool_name` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_m_pool`
--

INSERT INTO `tb_m_pool` (`pool_id`, `pool_name_id`, `pool_name`) VALUES
('01', '01-JKT', 'Pulo gadung'),
('02', '01-JKT', 'Gading');

-- --------------------------------------------------------

--
-- Table structure for table `tb_m_price`
--

CREATE TABLE `tb_m_price` (
  `price_id` int(10) NOT NULL,
  `price` int(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_m_route`
--

CREATE TABLE `tb_m_route` (
  `route_id` varchar(10) NOT NULL,
  `location_id` varchar(10) DEFAULT NULL,
  `price_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_m_seat`
--

CREATE TABLE `tb_m_seat` (
  `seat_id` int(10) NOT NULL,
  `bus_id` int(10) DEFAULT NULL,
  `seat_no` int(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_m_seat`
--

INSERT INTO `tb_m_seat` (`seat_id`, `bus_id`, `seat_no`) VALUES
(1, 1, 4);

-- --------------------------------------------------------

--
-- Table structure for table `tb_m_trip`
--

CREATE TABLE `tb_m_trip` (
  `trip_no` int(10) NOT NULL,
  `route_id` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_m_user`
--

CREATE TABLE `tb_m_user` (
  `user_id` int(10) NOT NULL,
  `user_name` varchar(25) DEFAULT NULL,
  `phone_no` varchar(255) DEFAULT NULL,
  `email` varchar(35) DEFAULT NULL,
  `password` varchar(35) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_m_user`
--

INSERT INTO `tb_m_user` (`user_id`, `user_name`, `phone_no`, `email`, `password`) VALUES
(1, 'Rizal Fahrudin', '082116162621', 'rizalfahrudin879@gmail.com', 'rizal'),
(2, 'tes', '0898', 'tes@g.com', 'tes'),
(3, 'annisa', 'tayo', 'tayo', 'tayo'),
(4, 'anisa', '0898', 'anisa@gmail', 'nisa'),
(5, 'niffari', '0812', 'niffari@gmail.com', 'niff'),
(6, 'ibnu', '888', 'ibnusofyan@gmail.com', 'sofyangans'),
(7, 'ann', '09191', 'aknask', 'ajaj'),
(8, 'ibnusofyan', '0812', 'ibnu@ganteng.com', 'ibnu'),
(9, 'oppa', '62662', 'oppa', 'oppa'),
(10, '08981044593', 'maria@gmail.com', 'mariaa', 'mariaa'),
(11, '0898181', 'brian@gmail.com', 'richbrian', 'brian55'),
(12, '8484884884844', 'der@gmail.com', 'rich', 'hdhdhdhhdhdhdh'),
(13, '0922', 'annisa@gmail.com', 'aniisa', 'aaaaaa'),
(14, 'firman', '09999', 'firman@gmail.com', 'firman');

-- --------------------------------------------------------

--
-- Table structure for table `tb_m_workday`
--

CREATE TABLE `tb_m_workday` (
  `workday_id` int(10) NOT NULL,
  `date_departure` date DEFAULT NULL,
  `date_arrival` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_m_workday`
--

INSERT INTO `tb_m_workday` (`workday_id`, `date_departure`, `date_arrival`) VALUES
(1, '2019-12-04', '2019-12-05');

-- --------------------------------------------------------

--
-- Table structure for table `tb_r_tiket`
--

CREATE TABLE `tb_r_tiket` (
  `tiket_id` int(10) NOT NULL,
  `user_id` int(10) DEFAULT NULL,
  `seat_id` int(10) DEFAULT NULL,
  `bus_id` int(10) DEFAULT NULL,
  `trip_no` int(10) DEFAULT NULL,
  `route_id` varchar(10) DEFAULT NULL,
  `workday_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_m_bus`
--
ALTER TABLE `tb_m_bus`
  ADD PRIMARY KEY (`bus_id`),
  ADD KEY `class_id` (`class_id`),
  ADD KEY `workday_id` (`workday_id`);

--
-- Indexes for table `tb_m_class`
--
ALTER TABLE `tb_m_class`
  ADD PRIMARY KEY (`class_id`);

--
-- Indexes for table `tb_m_location`
--
ALTER TABLE `tb_m_location`
  ADD PRIMARY KEY (`location_id`),
  ADD KEY `tb_m_location_ibfk_1` (`pool_id`);

--
-- Indexes for table `tb_m_pool`
--
ALTER TABLE `tb_m_pool`
  ADD PRIMARY KEY (`pool_id`);

--
-- Indexes for table `tb_m_price`
--
ALTER TABLE `tb_m_price`
  ADD PRIMARY KEY (`price_id`);

--
-- Indexes for table `tb_m_route`
--
ALTER TABLE `tb_m_route`
  ADD PRIMARY KEY (`route_id`),
  ADD KEY `price_id` (`price_id`),
  ADD KEY `location_id` (`location_id`);

--
-- Indexes for table `tb_m_seat`
--
ALTER TABLE `tb_m_seat`
  ADD PRIMARY KEY (`seat_id`),
  ADD KEY `bus_id` (`bus_id`);

--
-- Indexes for table `tb_m_trip`
--
ALTER TABLE `tb_m_trip`
  ADD PRIMARY KEY (`trip_no`),
  ADD KEY `route_id` (`route_id`);

--
-- Indexes for table `tb_m_user`
--
ALTER TABLE `tb_m_user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `tb_m_workday`
--
ALTER TABLE `tb_m_workday`
  ADD PRIMARY KEY (`workday_id`);

--
-- Indexes for table `tb_r_tiket`
--
ALTER TABLE `tb_r_tiket`
  ADD PRIMARY KEY (`tiket_id`),
  ADD KEY `bus_id` (`bus_id`),
  ADD KEY `trip_no` (`trip_no`),
  ADD KEY `route_id` (`route_id`),
  ADD KEY `seat_id` (`seat_id`),
  ADD KEY `workday_id` (`workday_id`),
  ADD KEY `user_id` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_m_user`
--
ALTER TABLE `tb_m_user`
  MODIFY `user_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tb_m_bus`
--
ALTER TABLE `tb_m_bus`
  ADD CONSTRAINT `tb_m_bus_ibfk_1` FOREIGN KEY (`class_id`) REFERENCES `tb_m_class` (`class_id`),
  ADD CONSTRAINT `tb_m_bus_ibfk_2` FOREIGN KEY (`workday_id`) REFERENCES `tb_m_workday` (`workday_id`);

--
-- Constraints for table `tb_m_location`
--
ALTER TABLE `tb_m_location`
  ADD CONSTRAINT `tb_m_location_ibfk_1` FOREIGN KEY (`pool_id`) REFERENCES `tb_m_pool` (`pool_id`);

--
-- Constraints for table `tb_m_route`
--
ALTER TABLE `tb_m_route`
  ADD CONSTRAINT `tb_m_route_ibfk_2` FOREIGN KEY (`price_id`) REFERENCES `tb_m_price` (`price_id`),
  ADD CONSTRAINT `tb_m_route_ibfk_3` FOREIGN KEY (`location_id`) REFERENCES `tb_m_location` (`location_id`);

--
-- Constraints for table `tb_m_seat`
--
ALTER TABLE `tb_m_seat`
  ADD CONSTRAINT `tb_m_seat_ibfk_1` FOREIGN KEY (`bus_id`) REFERENCES `tb_m_bus` (`bus_id`);

--
-- Constraints for table `tb_m_trip`
--
ALTER TABLE `tb_m_trip`
  ADD CONSTRAINT `tb_m_trip_ibfk_1` FOREIGN KEY (`route_id`) REFERENCES `tb_m_route` (`route_id`);

--
-- Constraints for table `tb_r_tiket`
--
ALTER TABLE `tb_r_tiket`
  ADD CONSTRAINT `tb_r_tiket_ibfk_1` FOREIGN KEY (`bus_id`) REFERENCES `tb_m_bus` (`bus_id`),
  ADD CONSTRAINT `tb_r_tiket_ibfk_2` FOREIGN KEY (`trip_no`) REFERENCES `tb_m_trip` (`trip_no`),
  ADD CONSTRAINT `tb_r_tiket_ibfk_3` FOREIGN KEY (`route_id`) REFERENCES `tb_m_route` (`route_id`),
  ADD CONSTRAINT `tb_r_tiket_ibfk_4` FOREIGN KEY (`seat_id`) REFERENCES `tb_m_seat` (`seat_id`),
  ADD CONSTRAINT `tb_r_tiket_ibfk_5` FOREIGN KEY (`workday_id`) REFERENCES `tb_m_workday` (`workday_id`),
  ADD CONSTRAINT `tb_r_tiket_ibfk_6` FOREIGN KEY (`user_id`) REFERENCES `tb_m_user` (`user_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
